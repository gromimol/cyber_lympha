$(document).ready(function() {

// popup
$('.js--popup').on('click', function(e) {
    e.preventDefault()

    $('body').removeClass('menu-open').addClass('noscroll')
    $('#overlay').show()
    $('.popup').addClass('active')
})

$('.close, #overlay').on('click', function(e) {
    e.preventDefault()
    e.stopPropagation()

    $('body').removeClass('noscroll').removeClass('menu-open')
    $('#overlay').hide()
    $('.popup').removeClass('active')
})

// mobile menu 
$('.burger').on('click', function(e) {
    e.preventDefault()
    e.stopPropagation()

    $('body').toggleClass('menu-open')
})

// отмена всплытие
$('.header__right').on('click', function(e) {
    e.stopPropagation()
})

// закрываем меню по клику вне элемента
$('html').on('click', function() {
    $('body').removeClass('menu-open')
})

// проверяем на наличие подменю
$('.menu > li').each(function(i) {
    if($(this).children('.submenu').length > 0) {
        $(this).addClass('has-submenu')
    }
})

// меню на мобильном.
if($(window).width() < 1201) {
    $('.has-submenu > a').on('click', function(e) {
        e.preventDefault()

        $(this).parent().toggleClass('submenu-open')
    })
}

// tabs 
$('.tabs__list li').on('click', function() {
    let tabId = $(this).data('tab')

    $(this).addClass('active').siblings().removeClass('active')
    $(this).closest('.tabs-wrapper').find('.tab').removeClass('active')
    $(`#${tabId}`).addClass('active')
})

// accordeon
$('.accordeon__title').on('click', function() {
    $(this).closest('.accordeon').toggleClass('active')
})

// slider
$('.in-box__slider').slick({
    autoplay: true,
    prevArrow: '<span class="prev-arrow"><svg width="16" height="31" viewBox="0 0 16 31" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15 1L0.999999 16.5914L15 30" stroke="currentColor"/></svg></span>',
    nextArrow: '<span class="next-arrow"><svg width="17" height="31" viewBox="0 0 17 31" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 1L16 16.5914L1 30" stroke="currentColor"/></svg></span>',
})

$('.news-slider').slick({
    // autoplay: true,
    prevArrow: '<span class="prev-arrow"><svg width="16" height="31" viewBox="0 0 16 31" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15 1L0.999999 16.5914L15 30" stroke="currentColor"/></svg></span>',
    nextArrow: '<span class="next-arrow"><svg width="17" height="31" viewBox="0 0 17 31" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 1L16 16.5914L1 30" stroke="currentColor"/></svg></span>',
})

// scroll
$(function() {
    let header = $('.header');
     
    $(window).scroll(function() {
      if($(this).scrollTop() > 100) {
       header.addClass('scrolled');
      } else {
       header.removeClass('scrolled');
      }
    });
   });

})